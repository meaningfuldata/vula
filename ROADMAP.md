# Roadmap

This is a rough description of the next changes that will be included in the project.

## [0.6.0]
 - [ ] Decide the definitive "props API" of components (what to do with prop/attribute `id`?).
 - [ ] Decide the definitive "props API" of components (cloning non-reactive tabs).
 - [ ] Decide the definitive "props API" of components (empty event).
 - [ ] Allow establishing the minimum and maximum size of panes.

## [0.7.0]
 - [ ] Maximize tabs.
 - [ ] Provide a way to obtain the layout data to make it reproducible.
 - [ ] Provide a way to set the layout data previously obtained.

## [0.8.0]
 - [ ] Allow dragging & dropping tabs within the same group.
 - [ ] Allow dragging & dropping tabs between different groups.

## [1.0.0]
 - [ ] Allow closing multiple tabs: to the left.
 - [ ] Allow closing multiple tabs: to the right.
 - [ ] Allow closing multiple tabs: other tabs.
 - [ ] Tab list.

## [1.x.y] - Nice to have
 - [ ] Extract mouse and drag & drop composables to new library (¿drapo?), to share them more easily.
 - [ ] Extract tabs component and composable to new library (¿vula-tabs?), to share them more easily.
 - [ ] Vertical tabs.
 - [ ] Left to right: tab position.
 - [ ] Do not include the default theme in the main bundle.
 - [ ] Keyboard.
 - [ ] Add events on mouseenter to optimize?
 - [ ] Lock layout.
 - [ ] Pass an object to tabs, layouts, or global configuration, to provide the default behaviour for tabs instead of defining it per event.
 - [ ] Option to reuse the content of previously active tabs instead of re-rendering them
 - [ ] Optimize finding tabs (to activate them).

## [x.y.z] - Nice to have
 - [ ] Constraints, namespaces or other idea to limit how components can interact.
 - [ ] Allow using tabs as direct children of `VulaWindow`.
 - [ ] Allow using tabs as direct children of `VulaLayout`.
 - [ ] Implement `CollapsiblePane` as an alternative to tabs.
 - [ ] Remove Lodash (now it is tree-shaked).
 - [ ] History, log or tracking.
