import { isAbsolute, relative, resolve } from 'node:path'

/**
 * Resolve `<root>/src/components/Component.ts` to `<root>/tests/unit/snapshot/components/Component.spec.snap.js`
 *
 * NOTE: `snapshotExtension` is ".snap"
 */
const resolveSnapshotPath = (testPath, snapshotExtension) => {
  const relativePath = isAbsolute(testPath)
    ? relative(resolve(testPath, __dirname, '../../src'), testPath)
    : testPath

  return `tests/unit/snapshots/${relativePath}`.replace(
    /\.ts$/,
    `${snapshotExtension}.js`
  )
}

export default resolveSnapshotPath
