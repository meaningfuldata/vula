// Vitest Snapshot v1, https://vitest.dev/guide/snapshot.html

exports[`VulaTab.vue > renders correctly 1`] = `
"<a class="vula__tab" id="tab-1" draggable="false"><span class="vula__tab__title">Tab 1 title</span>
  <!--v-if-->
</a>"
`;

exports[`VulaTab.vue renders correctly 1`] = `
"<a class=\\"vula__tab\\" id=\\"tab-1\\" draggable=\\"false\\"><span class=\\"vula__tab__title\\">Tab 1 title</span>
  <!--v-if-->
</a>"
`;
