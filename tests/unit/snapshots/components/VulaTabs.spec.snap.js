// Vitest Snapshot v1, https://vitest.dev/guide/snapshot.html

exports[`VulaTabs.vue > default slot > when contains one \`VulaTab\` as unique child > renders one tab 1`] = `
"<div class="vula__tabs">
  <nav class="vula__tabs__header vula--droppable">
    <vula-tab id="unique" title="unique">CONTENT</vula-tab>
  </nav>
  <div class="vula__tabs__content" style="display: none;"></div>
</div>"
`;

exports[`VulaTabs.vue > default slot > when contains several \`VulaTab\` > renders them 1`] = `
"<div class="vula__tabs">
  <nav class="vula__tabs__header vula--droppable">
    <vula-tab id="tab-1" title="One">CONTENT One</vula-tab>
    <vula-tab id="tab-2" title="Two">CONTENT Two</vula-tab>
    <vula-tab id="tab-3" title="Three">CONTENT Three</vula-tab>
  </nav>
  <div class="vula__tabs__content" style="display: none;"></div>
  <div class="vula__tabs__content" style="display: none;"></div>
  <div class="vula__tabs__content" style="display: none;"></div>
</div>"
`;

exports[`VulaTabs.vue > default slot > when contains several \`VulaTab\` with \`v-for\` > renders them 1`] = `
"<div class="vula__tabs">
  <nav class="vula__tabs__header vula--droppable">
    <vula-tab id="tab-1" title="Tab 1">CONTENT 1</vula-tab>
    <vula-tab id="tab-2" title="Tab 2">CONTENT 2</vula-tab>
    <vula-tab id="tab-3" title="Tab 3">CONTENT 3</vula-tab>
  </nav>
  <div class="vula__tabs__content" style="display: none;"></div>
  <div class="vula__tabs__content" style="display: none;"></div>
  <div class="vula__tabs__content" style="display: none;"></div>
</div>"
`;

exports[`VulaTabs.vue default slot when contains one \`VulaTab\` as unique child renders one tab 1`] = `
"<div class=\\"vula__tabs\\">
  <nav class=\\"vula__tabs__header vula--droppable\\">
    <vula-tab id=\\"unique\\" title=\\"unique\\">CONTENT</vula-tab>
  </nav>
  <div class=\\"vula__tabs__content\\" style=\\"display: none;\\"></div>
</div>"
`;

exports[`VulaTabs.vue default slot when contains several \`VulaTab\` renders them 1`] = `
"<div class=\\"vula__tabs\\">
  <nav class=\\"vula__tabs__header vula--droppable\\">
    <vula-tab id=\\"tab-1\\" title=\\"One\\">CONTENT One</vula-tab>
    <vula-tab id=\\"tab-2\\" title=\\"Two\\">CONTENT Two</vula-tab>
    <vula-tab id=\\"tab-3\\" title=\\"Three\\">CONTENT Three</vula-tab>
  </nav>
  <div class=\\"vula__tabs__content\\" style=\\"display: none;\\"></div>
  <div class=\\"vula__tabs__content\\" style=\\"display: none;\\"></div>
  <div class=\\"vula__tabs__content\\" style=\\"display: none;\\"></div>
</div>"
`;

exports[`VulaTabs.vue default slot when contains several \`VulaTab\` with \`v-for\` renders them 1`] = `
"<div class=\\"vula__tabs\\">
  <nav class=\\"vula__tabs__header vula--droppable\\">
    <vula-tab id=\\"tab-1\\" title=\\"Tab 1\\">CONTENT 1</vula-tab>
    <vula-tab id=\\"tab-2\\" title=\\"Tab 2\\">CONTENT 2</vula-tab>
    <vula-tab id=\\"tab-3\\" title=\\"Tab 3\\">CONTENT 3</vula-tab>
  </nav>
  <div class=\\"vula__tabs__content\\" style=\\"display: none;\\"></div>
  <div class=\\"vula__tabs__content\\" style=\\"display: none;\\"></div>
  <div class=\\"vula__tabs__content\\" style=\\"display: none;\\"></div>
</div>"
`;
