import { createApp } from 'vue'
import App from './App.vue'
import '../src/styles/index.styl'
import '../src/styles/themes/default.styl'

createApp(App).mount('#app')
