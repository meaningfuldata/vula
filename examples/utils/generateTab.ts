import type { Tab } from '@/types/'

let tabCount = 0

export default function generateTab(options = {}): Tab {
  return {
    ...options,
    id: `example-tab-${++tabCount}`,
    title: `Tab ${tabCount}`,
    isClosable: true,
    isDraggable: true
  }
}
