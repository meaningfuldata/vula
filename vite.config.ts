import { resolve } from 'node:path'
/// <reference types="vitest/config" />
import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import type { OutputOptions } from 'rollup'
import resolveSnapshotPath from './tests/unit/snapshotResolver'

const globals = {
  vue: 'Vue'
}
const output: OutputOptions = {
  inlineDynamicImports: true,
  exports: 'named',
  globals,
  assetFileNames: (assetInfo: any) => {
    if (assetInfo.name == 'style.css') {
      return 'vula.css'
    }
    return assetInfo.name
  }
}

export default defineConfig({
  resolve: {
    alias: {
      '@': resolve(__dirname, './src'),
      // This bundle of Lodash can be tree-shaked
      lodash: 'lodash-es'
    }
  },

  build: {
    lib: {
      name: 'Vula',
      entry: resolve(__dirname, './src/index.ts')
    },
    rollupOptions: {
      external: ['vue'],
      output: [
        {
          ...output,
          format: 'es',
          esModule: true
        },
        {
          ...output,
          format: 'umd',
          interop: 'esModule'
        }
      ]
    }
  },

  plugins: [vue()],

  test: {
    // Avoid trying to include files from incorrect places, such as `.direnv`
    include: ['src/**/*.spec.(j|t)s', 'tests/**/*.spec.(j|t)s'],

    // Use a browser-like environment
    environment: 'happy-dom',

    resolveSnapshotPath
  }
})
