# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

### Changed

### Fixed

### Removed

## [0.5.3] - 2024-11-28

### Added

- Create the Nix flake.

### Changed

- Replace Jest with Vitest.

### Fixed

- Fix some problems when tabs are not rendered initially or they are removed.

### Removed

## [0.5.2] - 2021-06-27

### Fixed

- Set the height and width of empty containers to 100%.
- Show the previously closed tabs when re-opened.

## [0.5.1] - 2021-04-28

### Fixed

- Traverse `VNode`s that look like fragments even when they type is not declared.

## [0.5.0] - 2021-04-22

This release fixes a lots of small issues with the tabs and some of the panes, and stabilizes a little more their usage.

### Added

- Emit the event `activated` when a tab has been activated in `VulaTabs`.
- Emit the event `closed` when a tab has been closed in `VulaTabs`.
- Emit the event `opened` when a tab has been opened in `VulaTabs`.
- Implement the `add` method of `useTabs`.
- Add `build:ts` script to build TypeScript definitions alone.
- Add `lint:ts` script to check types.
- Generate TypeScript definitions when building the bundle.

### Changed

- Allow not specifying the direction of the `VulaContainer` if it is not split.
- Remove the margin around `.vula__tabs > nav` and the padding inside `vula__tabs__content` of the default theme.
- Emit full `Tab` objects instead of `ID` in all tab events.
- Refactor the `useTabs` composable to use `Set<ID>` instead of arrays.
- Trigger effects when using `wasActive` and `wasRemoved`, from the `useTabs` composable.
- Inserting tabs in the reactive `tabs` of `useTabs` does not activate them automatically anymore.
  That default behaviour complicated a lot adding tabs in the background, without activating them.
  The old behaviour could be replicated easily by adding a tab and then activating it, or using `add` method.
- Reduce the `z-index` of `.vula__divider` to 10 to reduce possible conflicts with other libraries.

### Fixed

- Do not fail when using non PascalCase names for `VulaPane` and `VulaDivider` inside `VulaContainer`.
- React to changing the value of the `active` prop of `VulaTabs`.
- Ignore comments inside `VulaContainer`.
- Set the initial active tab as expected, or the last if it is not explicitly established.
- Do not throw an `Error` when trying to remove the first tab in `useTabs`.
- Do not render the first tab content unless it is the active one.
- Fix all TypeScript errors.
- Set the active tab when using `v-if` on `VulaTab` siblings.

## [0.4.0] - 2021-02-10

### Added

- Allow using `horizontal` and `vertical` instead of the `direction` prop.
- Treat layouts as containers so they could be divided in panes.
- Allow using the slots `title` and `close` to customize `VulaTab`.

### Changed

- Instead of passing a `pane` prop, of type `Pane`, use its properties as props of `VulaPane`.
- Store the state of tab groups inside the layout.
- Use the name of the container component (`VulaLayout` or `VulaPane`) on errors.
- Improve the default theme for tabs.

### Fixed

- Display the content on the tab when it is active.
- The tab content is rendered once, not every time its tab is activated.

### Removed

- Namespaces have been removed since the API is not stable enough to include that feature.
- Removed `useTabGroups` composable. Its functionality will be in `useTabs`.

## [0.3.1] - 2021-12-21

### Added

- Include handles on the default theme of the pane dividers to remark that they can be dragged.

## [0.3.0] - 2021-12-21

**First release**
