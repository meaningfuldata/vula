{
  description = ''
    A Nix flake for Vula.
  '';
  
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-24.05";
    # Some utilities
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { nixpkgs, flake-utils, ... }: flake-utils.lib.eachDefaultSystem(system:
    let
      project = {
        name = "vula";
        title = "Vula";
      };

      pkgs = import nixpkgs {
        inherit system;
      };

      # Packages used for development
      depsPackages = with pkgs; [
        nodejs_18
        yarn
      ];
    in
    {
      devShells = {
        # Default shell
        default = pkgs.mkShell {
          packages = depsPackages;

          shellHook = ''
            echo -e "\n\tEntering ${project.title}\n"
          '';
        };
      };
    });
}
