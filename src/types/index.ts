import type { Ref, UnwrapNestedRefs as Reactive } from 'vue'

// FIXME: the prop `id` is also an HTML attribute:
// - [ ] Rename `id` to `<x>Id`
// - [ ] Rename `id` to `label`
// - [ ] Rename `id` to `value`

export type MaybeReactive<T> = T | Reactive<T>
export type MaybeRef<T> = T | Ref<T>

export type ID = string

/**
 * An entity that can be anonymous (no ID) or not.
 */
export interface Identifiable {
  id?: ID
}

/**
 * An entity that id identified.
 */
export interface Identified {
  readonly id: ID
}

/**
 * A type for declaring possible anonymous entities.
 */
export type Anonymous<T> = Omit<T, 'id'> & Identifiable

/**
 * User defined values.
 * As an example, in Vula tabs, the `data` property holds the pane ID to group
 * the tabs by pane.
 */
interface Customizable {
  data?: any
}

/**
 * Direction dtermines how the pane can be splittled: horizontally (row) or vertically (column).
 */
export type Direction = 'horizontal' | 'vertical'

interface Coordinates {
  x: number
  y: number
  z: number
}

interface Dimensions {
  height: number
  width: number
}

interface Area extends Coordinates, Dimensions {}

interface Draggable {
  /**
   * The object can be dragged.
   * False by default.
   * When enabled, the default header can be used drag the object to other position.
   */
  isDraggable?: boolean
}

interface Droppable {
  isDroppable?: boolean
}

interface ContainerDirection {
  direction: Direction
}

// Use either panes or slots
// TODO: refactor to type Either
interface OnlyPanes {
  panes: Array<Pane>
  slots?: never
}
interface OnlySlots {
  panes?: never
  slots: Array<any>
}

/**
 * Container can be divided into panes along an axis.
 * FIXME: change to not include panes or slots
 */
export type Container = ContainerDirection // & (OnlyPanes | OnlySlots)

/**
 * Layout is the root container
 */
export type Layout = Identified & Container

/**
 * Windows can be split in panes.
 */
export type Window = Identified & Container & Area & Draggable

interface PaneSize {
  /**
   * By default, the size is calculated automatically by the browser.
   */
  size?: number
}

/**
 * Pane is an area inside a `Window`.
 */
export type Pane = Identified & Container & PaneSize

// TODO: generic with data/ content / component
// TODO: id and title are HTML attributes
export interface Tab extends Identified, Customizable, Draggable, Droppable {
  /**
   * Tab can be closed.
   * False by default.
   * When enabled, the default header includes a button to close the tab.
   */
  isClosable?: boolean
  /**
   * The tab is destoyed each time is hidden (not active).
   */
  isVolatile?: boolean
  /**
   * Title of the tab.
   * It is used as the label on the default tab header.
   * It is optional when using a custom header.
   */
  title?: string
  /**
   * Link to route.
   * TODO: deprecate and remove
   */
  to?: any
}

/**
 * Tabs can be queried by their properties, including the `data` user-defined
 * property, of by a function.
 */
export type TabQuery = Record<string, any> | ((tab: Tab) => boolean)

interface Tabs {
  /**
   * Active tab, or `null` if the `tabs` array is empty or none has been initialised.
   */
  active?: null | Tab
  tabs: Array<Tab>
}

/**
 * TODO: currently an ID is not provided since it wouldn't be used
 * TODO: tabs could be anonymous in some cases
 */
export interface TabGroup extends Tabs {}
