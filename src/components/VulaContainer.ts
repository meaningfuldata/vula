import { defineComponent, h } from 'vue'
import type { PropType, VNode } from 'vue'
import { VulaDivider } from '@/components/'
import type { Direction, ID } from '@/types/'
import {
  clamp,
  clampPercentage,
  generateId,
  isNil,
  isSymbol,
  parsePercentage,
  pascalCase,
  logError
} from '@/utils/'

type Dimension = 'height' | 'width'

interface DirectionOptions {
  // TODO: Duplicated instead of reusing from `@types` due a Vue SFC compiler limitation
  direction?: Direction
  horizontal?: boolean
  vertical?: boolean
}

const abcissaMeasures = {
  start: 'left',
  end: 'right',
  length: 'width',
  cursor: 'clientX'
}
const ordinateMeasures = {
  start: 'top',
  end: 'bottom',
  length: 'height',
  cursor: 'clientY'
}

type Measures = typeof abcissaMeasures | typeof ordinateMeasures

export default defineComponent({
  // Additional CSS classes used by the container
  componentClasses: '',

  props: {
    id: {
      type: String as PropType<ID>,
      // TODO: different prefix by subclass (layout, pane)
      default: () => generateId()
    },
    direction: {
      type: String as PropType<Direction>,
      default: undefined,
      validator(value: Direction): boolean {
        return ['horizontal', 'vertical'].includes(value)
      }
    },
    horizontal: {
      type: Boolean as PropType<boolean>,
      default: undefined
    },
    vertical: {
      type: Boolean as PropType<boolean>,
      default: undefined
    }
  },

  computed: {
    panesDirection(): Direction | null {
      ;['direction', 'horizontal', 'vertical'].reduce((count, prop) => {
        if (
          !isNil(this.$props[prop as keyof DirectionOptions]) &&
          ++count > 1
        ) {
          throw new Error(
            `\`${this.$options.name}\` can use exclusively \`direction\`, \`horizontal\` or \`vertical\``
          )
        }
        return count
      }, 0)

      return (
        this.direction ||
        (this.horizontal && 'horizontal') ||
        (this.vertical && 'vertical') ||
        null
      )
    },

    isHorizontal(): boolean {
      return this.panesDirection === 'horizontal'
    },

    panesDimension(): Dimension {
      return this.isHorizontal ? 'width' : 'height'
    },

    panesMeasures(): Measures {
      return this.isHorizontal ? abcissaMeasures : ordinateMeasures
    }
  },

  methods: {
    // TODO: min and max pane size
    // TODO: when all panes have size it should equal 100%
    // TODO: precission error
    checkPaneSize(size: number, totalSize: number): void {
      if (size <= 0) {
        logError('The minimum size of `VulaPane` cannot be less or equal 0%')
      }

      if (totalSize > 100) {
        logError('The sum of all `VulaPane` cannot exceeed 100%')
      }
    },

    /**
     * Event triggered when a divider is moved
     */
    onResize(
      event: MouseEvent,
      paneBefore: HTMLElement,
      paneAfter: HTMLElement
    ): void {
      const areaBefore = paneBefore.getBoundingClientRect(),
        areaAfter = paneAfter.getBoundingClientRect()

      const cursorAbsolute = event[
          this.panesMeasures.cursor as keyof MouseEvent
        ] as number,
        beforeStart = areaBefore[
          this.panesMeasures.start as keyof DOMRect
        ] as number,
        beforeLenght = areaBefore[
          this.panesMeasures.length as keyof DOMRect
        ] as number,
        afterEnd = areaAfter[this.panesMeasures.end as keyof DOMRect] as number

      // NOTE: `containerEnd` = `beforeLenght` + <divider thickness>
      const containerStart = 0,
        containerEnd = afterEnd - beforeStart

      // Position of the cursor (the divider) relative to the previous pane
      const cursorRelative = cursorAbsolute - beforeStart,
        dividerPosition = clamp(cursorRelative, containerStart, containerEnd)

      const beforeSize = parsePercentage(paneBefore.style[this.panesDimension])
      let percentageMoved = dividerPosition / beforeLenght,
        beforeNewSize = clampPercentage(beforeSize * percentageMoved)

      // The pane size has been resized to 0
      if (isNaN(percentageMoved)) {
        beforeNewSize = 0

        // The pane size is 0
      } else if (!isFinite(percentageMoved)) {
        beforeNewSize = cursorRelative / 100
      }

      const afterSize = parsePercentage(paneAfter.style[this.panesDimension])
      // FIXME: This percentage should be calculated to generate the exact same
      // `left` or `top` position, so the pane is not moved even a pixel
      const afterNewSize = clampPercentage(
        afterSize - (beforeNewSize - beforeSize)
      )

      paneBefore.style[this.panesDimension] = `${beforeNewSize}%`
      paneAfter.style[this.panesDimension] = `${afterNewSize}%`

      // FIXME: emit the size of all sub-panes
      // emit('resize', [])
    }
  },

  // TODO: react to changes on props (direction, etc.)
  render() {
    const slotNodes: Array<VNode> =
      (this.$slots?.default && this.$slots.default()) || []

    // Assume that there are not panes until finding one
    let hasPanes = false

    const components: Array<VNode> = []

    if (slotNodes.length) {
      const dividers = slotNodes.length - 1
      let totalSize = 0,
        componentIndex = 0

      const push = (component: VNode | typeof VulaDivider, props: any = {}) => {
        components.push(h(component, props))
        componentIndex++
      }

      const isComment = (slot: VNode): boolean => {
        return isSymbol(slot.type) && slot.type.toString() === 'Symbol(Comment)'
      }

      slotNodes.forEach((slot: VNode, slotIndex: number) => {
        const containerComponents = ['VulaPane', 'VulaDivider']

        // The slot is a comment
        if (isComment(slot)) {
          push(slot)

          // When the slot does not contain sub-panes
        } else if (
          containerComponents.indexOf(pascalCase((slot.type as any).name)) ===
          -1
        ) {
          if (hasPanes) {
            const elementName = (slot.type as any).name
            throw new Error(
              `\`${this.$options.name}\` cannot include panes and dividers along with other elements (\`${elementName}\`)`
            )
          }

          push(slot)
        } else {
          if (!this.panesDirection) {
            throw new Error(
              `\`${this.$options.name}\` with ID ${this.id} requires a direction because it contains other panes`
            )
          }

          hasPanes = true

          const size = slot.props?.size
          // TODO: either width or height
          const style = {} as CSSStyleDeclaration

          if (typeof size === 'number') {
            totalSize += size
            this.checkPaneSize(size, totalSize)

            style[this.panesDimension] = `${size}%`
          }
          push(slot, { style })

          if (slotIndex < dividers) {
            push(VulaDivider, {
              // Closure to capture the index of the divider inside the loop, and then,
              // when it is moved, resize its previous and subsequent panes
              onMove: (index => (event: MouseEvent) => {
                const paneBefore = components[index - 1].el as HTMLElement,
                  paneAfter = components[index + 1].el as HTMLElement

                this.onResize(event, paneBefore, paneAfter)
              })(componentIndex)
            })
          }
        }
      })
    }

    // NOTE: direction is optional when the container is not split
    const classes = hasPanes
      ? `vula__container--${this.panesDirection} ${this.$options.componentClasses}`
      : `vula__container ${this.$options.componentClasses}`

    const props = {
      id: this.id,
      class: classes
    }
    return h('div', props, components)
  }
})
