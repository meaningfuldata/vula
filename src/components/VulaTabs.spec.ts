import { beforeEach, beforeAll, afterAll, expect, it, describe } from 'vitest'
import { h } from 'vue'
import { flushPromises, mount } from '@vue/test-utils'
import { VulaTabs } from './'
import sinon from 'sinon'

describe('VulaTabs.vue', () => {
  let wrapper: any

  beforeAll(() => sinon.stub(crypto, 'getRandomValues').returns([0]))
  afterAll(() => sinon.resetBehavior())

  const wrap = (options: any = {}) => {
    wrapper = mount(VulaTabs, options)
  }

  const wrapDefaultSlot = (defaultSlot: any) => {
    wrap({
      slots: { default: defaultSlot }
    })
  }

  describe('default slot', () => {
    describe('when it is empty', () => {
      it('throws an `Error`', () => {
        expect(() => {
          wrap()
        }).to.throw(
          '`VulaTabs` requires using the default slot to define the tabs'
        )
      })
    })

    describe('when contains text', () => {
      it.skip('throws an `Error`', () => {
        expect(() => {
          wrapDefaultSlot('DEFAULT SLOT')
        }).to.throw(
          '`VulaTabs` requires using the default slot to define the tabs'
        )
      })
    })

    describe('when contains HTML elements', () => {
      it.skip('throws an `Error`', () => {
        expect(() => {
          wrapDefaultSlot('<h1>DEFAULT SLOT</h1>')
        }).to.throw(
          '`VulaTabs` requires using the default slot to define the tabs'
        )
      })
    })

    describe('when contains one `VulaTab` as unique child', () => {
      beforeEach(() =>
        wrapDefaultSlot(
          '<vula-tab id="unique" title="unique">CONTENT</vula-tab>'
        )
      )

      it('renders one tab', () => {
        expect(wrapper.html()).to.matchSnapshot()
      })
    })

    describe('when contains several `VulaTab`', () => {
      beforeEach(() =>
        wrapDefaultSlot([
          '<vula-tab id="tab-1" title="One">CONTENT One</vula-tab>',
          '<vula-tab id="tab-2" title="Two">CONTENT Two</vula-tab>',
          '<vula-tab id="tab-3" title="Three">CONTENT Three</vula-tab>'
        ])
      )

      it('renders them', () => {
        expect(wrapper.html()).to.matchSnapshot()
      })
    })

    describe('when contains several `VulaTab` with `v-for`', () => {
      beforeEach(() =>
        wrapDefaultSlot(
          '<vula-tab v-for="i in [1, 2, 3]" :id="`tab-${i}`" :title="`Tab ${i}`">CONTENT {{ i }}</vula-tab>'
        )
      )

      it('renders them', () => {
        expect(wrapper.html()).to.matchSnapshot()
      })
    })
  })

  describe.skip('slot `empty`', () => { })

  describe('when rendering the tabs', () => {
    describe('when a tab is active', () => {
      const defaulSlotWithTabs =
        '<vula-tab v-for="i in [1, 2, 3]" :id="`tab-${i}`" :title="`Tab ${i}`">CONTENT {{ i }}</vula-tab>'

      beforeEach(() =>
        wrap({
          props: {
            active: 'tab-2'
          },
          slots: {
            default: defaulSlotWithTabs
          }
        })
      )

      // FIXME: content is not rendered
      it.skip('that tab content is rendered', async () => {
        await flushPromises()
        const tabContent = await wrapper.find('.vula__tabs__content')
        await flushPromises()
        expect(tabContent.exists()).to.be.true
        expect(tabContent.html()).to.contain('CONTENT 2')
      })

      // FIXME: content is not rendered
      it.skip('that tab content is visible', async () => {
        await flushPromises()
        const tabContent = await wrapper.find('.vula__tabs__content')
        await flushPromises()
        expect(tabContent.isVisible()).to.be.true
      })

      it.skip('renders an empty placeholder for all the tabs except the active one', () => {
        wrap({ slots: { default: () => [h('div')] } })
      })
    })

    it('emits the `activated` event once after having rendered all', () => {
      expect(wrapper.emitted('activated')).to.not.be.ok
    })
  })

  describe.skip('when a tab is activated', () => {
    it('emits the `activated` event with the full `Tab`', () => {
      expect(wrapper.emitted('activated')).to.be.ok
    })

    it('hides the previous active tab', () => { })
    it('does not destroy the previous active tab', () => { })
  })

  describe.skip('when a tab is closed', () => {
    it('emits the `closed` event with the full `Tab`', () => {
      expect(wrapper.emitted('closed')).to.be.ok
    })

    it('destroys the closed tab', () => { })
  })
})
