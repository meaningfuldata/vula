import { mount } from '@vue/test-utils'
import { beforeEach, expect, it, describe } from 'vitest'
import { VulaDivider } from './'

describe('VulaDivider.vue', () => {
  let wrapper: any

  beforeEach(() => {
    wrapper = mount(VulaDivider, {})
  })

  it('renders correctly', () => {
    expect(wrapper.html()).to.matchSnapshot()
  })
})
