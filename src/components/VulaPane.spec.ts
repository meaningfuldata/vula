import { beforeEach, afterEach, expect, it, describe } from 'vitest'
import { mount } from '@vue/test-utils'
import { VulaPane } from './'
import { Pane } from '@/types/'
import sinon from 'sinon'

const pane: Pane = {
  id: 'tab-1',
  direction: 'vertical'
}

describe('VulaPane.vue', () => {
  let wrapper: any

  beforeEach(() => sinon.stub(crypto, 'getRandomValues').returns([0]))
  afterEach(() => sinon.resetBehavior())

  beforeEach(() => {
    wrapper = mount(VulaPane, {
      props: { ...pane }
    })
  })

  it('renders correctly', () => {
    expect(wrapper.html()).to.matchSnapshot()
  })

  describe.skip('default slot', () => {
    describe('when it is empty', () => {
      it('renders correctly', () => {
        expect(wrapper.html()).to.matchSnapshot()
      })
    })

    describe('when contains text', () => {
      beforeEach(() => {
        wrap({ slots: { default: 'Example text' } })
      })

      it('renders correctly', () => {
        expect(wrapper.html()).to.matchSnapshot()
      })
    })

    describe('when contains HTML elements', () => {
      beforeEach(() => {
        wrap({
          slots: { default: '<h2>Title</h2><b>Example text</b>' }
        })
      })

      it('renders correctly', () => {
        expect(wrapper.html()).to.matchSnapshot()
      })
    })

    describe('when contains one `VulaPane`', () => {
      beforeEach(() => wrap({ slots: { default: '' } }))
    })

    describe('when contains several `VulaPane`', () => { })

    describe('when contains several `VulaPane` with `v-for`', () => { })
  })
})
