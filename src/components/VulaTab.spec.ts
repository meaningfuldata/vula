import {
  beforeEach,
  beforeAll,
  afterAll,
  expect,
  it,
  describe,
  test
} from 'vitest'
import { mount } from '@vue/test-utils'
import { VulaTab } from './'
import type { Tab } from '@/types/'
import { INJECTION_KEY_TAB_GROUP } from '@/constants/'
import sinon from 'sinon'

const tab: Tab = {
  id: 'tab-1',
  title: 'Tab 1 title'
}

describe('VulaTab.vue', () => {
  let wrapper: any

  beforeAll(() => sinon.stub(crypto, 'getRandomValues').returns([0]))
  afterAll(() => sinon.resetBehavior())

  const defaultOptions = {
    props: { ...tab },
    global: {
      provide: {
        [INJECTION_KEY_TAB_GROUP]: {
          tabs: [],
          active: null
        }
      }
    }
  }

  const wrap = (options: any = {}) => {
    wrapper = mount(VulaTab, { ...defaultOptions, ...options })
  }

  beforeEach(wrap)

  it('renders correctly', () => {
    expect(wrapper.html()).to.matchSnapshot()
  })

  describe.skip('Props: default values', () => {
    test.todo('generates the ID')
  })

  describe.skip('when the tab group is provided', () => {
    test.todo('appends the props as `Tab`')
  })

  describe('when the tab is closable', () => {
    describe('when the slot `close` is defined', () => {
      beforeEach(() =>
        wrap({
          props: {
            ...tab,
            isClosable: true
          },
          slots: {
            close: 'CLOSE SLOT'
          }
        })
      )

      it('renders that slot', () => {
        expect(wrapper.html()).to.contain('CLOSE SLOT')
      })

      it('does not render the close button', () => {
        expect(wrapper.find('.vula__tab__close').exists()).to.be.false
      })
    })

    describe('when the slot `close` is not defined', () => {
      beforeEach(() =>
        wrap({
          props: {
            ...tab,
            isClosable: true
          }
        })
      )

      it('renders the close button', () => {
        expect(wrapper.find('.vula__tab__close').exists()).to.be.true
      })

      describe('when the close button is clicked', () => {
        it('emits the `close` event with the full `Tab`', () => {
          expect(wrapper.emitted('close')).to.not.be.ok
          const close = wrapper.find('.vula__tab__close')

          const fullTab: Tab = {
            ...tab,
            isClosable: true,
            isDraggable: false,
            isDroppable: false
          }
          close.trigger('click')
          expect(wrapper.emitted('close')).to.be.ok
          expect(wrapper.emitted('close')[0]).to.deep.equal([fullTab])
        })
      })
    })
  })

  describe('when the tab is not closable', () => {
    describe('when the slot `close` is not defined', () => {
      it('does not render the close button', () => {
        expect(wrapper.find('.vula__tab__close').exists()).to.be.false
      })
    })

    describe('when the slot `close` is defined', () => {
      beforeEach(() =>
        wrap({
          slots: {
            close: 'CLOSE SLOT'
          }
        })
      )

      it('does not render that slot', () => {
        expect(wrapper.html()).to.not.contain('CLOSE SLOT')
      })
    })
  })

  describe('when the slot `title` is defined', () => {
    beforeEach(() => {
      wrap({
        slots: {
          title: 'TITLE SLOT'
        }
      })
    })

    it('renders that slot', () => {
      expect(wrapper.html()).to.contain('TITLE SLOT')
    })

    it('does not render the text of the `title` prop', () => {
      expect(wrapper.find('.vula__tab__title').exists()).to.be.false
    })
  })

  describe('when the slot `title` is not defined', () => {
    it('renders the text of the `title` prop', () => {
      expect(wrapper.find('.vula__tab__title').text()).to.equal(tab.title)
    })
  })

  describe('when the tab is clicked', () => {
    describe('when the tab is not active', () => {
      it('emits the `activate` event', () => {
        expect(wrapper.emitted('activate')).to.not.be.ok
        const tabHeader = wrapper.find('.vula__tab')

        const fullTab: Tab = {
          ...tab,
          isClosable: false,
          isDraggable: false,
          isDroppable: false
        }
        tabHeader.trigger('click')
        expect(wrapper.emitted('activate')).to.be.ok
        expect(wrapper.emitted('activate')[0]).to.deep.equal([fullTab])
      })
    })

    describe('when the tab is already active', () => {
      beforeEach(() =>
        wrap({
          global: {
            provide: {
              [INJECTION_KEY_TAB_GROUP]: {
                tabs: [],
                active: {
                  id: tab.id
                }
              }
            }
          }
        })
      )

      it('does not emit anything', () => {
        expect(wrapper.emitted('activate')).to.not.be.ok
        const tabHeader = wrapper.find('.vula__tab')

        tabHeader.trigger('click')
        expect(wrapper.emitted('activate')).to.not.be.ok
      })
    })
  })
})
