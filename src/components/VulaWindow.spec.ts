import { beforeEach, afterEach, expect, it, describe } from 'vitest'
import { mount } from '@vue/test-utils'
import { VulaWindow } from './'
import { Window } from '@/types/'
import sinon from 'sinon'

const window: Window = {
  id: 'window-1',
  x: 0,
  y: 0,
  z: 0,
  height: 50,
  width: 50,
  direction: 'vertical'
}

describe('VulaWindow.vue', () => {
  let wrapper: any

  beforeEach(() => sinon.stub(crypto, 'getRandomValues').returns([0]))
  afterEach(() => sinon.resetBehavior())

  beforeEach(() => {
    wrapper = mount(VulaWindow, {
      props: { window }
    })
  })

  it('renders correctly', () => {
    expect(wrapper.html()).to.matchSnapshot()
  })
})
