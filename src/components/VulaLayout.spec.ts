import { beforeEach, afterEach, expect, it, describe } from 'vitest'
import { mount } from '@vue/test-utils'
import { VulaLayout } from './'
import { Layout } from '@/types/'
import sinon from 'sinon'

const layout: Layout = {
  id: 'layout-1',
  direction: 'horizontal'
}

describe('VulaLayout.vue', () => {
  let wrapper: any

  beforeEach(() => sinon.stub(crypto, 'getRandomValues').returns([0]))
  afterEach(() => sinon.resetBehavior())

  beforeEach(() => {
    wrapper = mount(VulaLayout, {
      props: { ...layout }
    })
  })

  it('renders correctly', () => {
    expect(wrapper.html()).to.matchSnapshot()
  })
})
