import { defineComponent } from 'vue'
import type { PropType } from 'vue'
import VulaContainer from './VulaContainer'

type Sizes = Array<number>

export default defineComponent({
  name: 'VulaPane',

  extends: VulaContainer,

  componentClasses: 'vula__pane',

  props: {
    size: {
      type: Number as PropType<number>,
      default: undefined
    }
  },

  // TODO: emit events
  emits: {
    resize: (_payload: Sizes) => true,
    resizeStart: (_payload: Sizes) => true,
    resizeEnd: (_payload: Sizes) => true
  }
})
