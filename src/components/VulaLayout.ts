import { defineComponent, provide, reactive } from 'vue'
import VulaContainer from './VulaContainer'
import type { TabGroup } from '@/types/'
// import type { Layout, TabGroup } from '@/types/'
// import { deepIdentify } from '@/utils/'

export default defineComponent({
  name: 'VulaLayout',

  extends: VulaContainer,

  componentClasses: 'vula__layout vula__theme__default',

  // TODO: react to changes on props (direction, etc.)
  setup(props) {
    // Provide the `layout` to each rendered `vula-tabs`
    provide('layout', {
      tabGroups: reactive([] as Array<TabGroup>)
    })

    // FIXME: not really necessary now
    // const layout: Layout = { id: props.id } as Layout
    // deepIdentify(layout)
  }
})
