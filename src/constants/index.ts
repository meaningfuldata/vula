import type { InjectionKey } from 'vue'
import type { TabGroup } from '@/types/'

// Drag & drop data types
const DATA_TYPES = { TAB: 'vula/tab' }

const INJECTION_KEY_TAB_GROUP = Symbol('tab-group') as InjectionKey<TabGroup>

export { DATA_TYPES, INJECTION_KEY_TAB_GROUP }
