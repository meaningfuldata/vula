import { generateId, isNil } from '.'
import type { Identified, Identifiable } from '@/types/'

/**
 * Modify deeply, in place, the objects to add the ID if it is not present.
 *
 * TODO: option to check duplicates
 */
export default function deepIdentify<
  T = Identifiable | Array<Identifiable>,
  S = Identified | Array<Identified>
>(target: T): S {
  if (Array.isArray(target)) {
    target.forEach(deepIdentify)
  } else if (isNil((target as Identifiable).id)) {
    ;(target as Identifiable).id = generateId()
  }
  return target as unknown as S
}
