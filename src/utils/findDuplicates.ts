export default function findDuplicates(items: Array<any>): Array<any> {
  const duplicates = items.filter((item, i) => items.indexOf(item) !== i)
  return [...new Set(duplicates)]
}
