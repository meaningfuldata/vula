/**
 * Parse percentage as float ("50.5%" => `50.5`)
 */
export default function (percentage: string): number {
  return parseFloat(percentage.slice(0, percentage.length - 1))
}
