import { camelCase, upperFirst } from 'lodash'

/**
 * Convert a string to PascalCase.
 */
export default function pascalCase(str: string): string {
  return upperFirst(camelCase(str))
}
