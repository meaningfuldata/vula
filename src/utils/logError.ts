export default function logError (message: string): void {
  console.error(`[vula] ${message}`)
}
