import clamp from './clamp'

export default function (percentage: number, min = 0, max = 100): number {
  return clamp(percentage, min, max)
}
