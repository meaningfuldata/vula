import type { ID } from '@/types/'

/**
 * Use crypto to get a random ID
 * @see https://developer.mozilla.org/en-US/docs/Web/API/Crypto/getRandomValues
 */
// TODO: it sould be very, very hard to generate duplicates, but to be acurate,
// this case should be managed. The method could have in mind user-defined IDs,
// which are probably more error-prone than this random generated value.
export default function generateId(prefix: string = 'vula'): ID {
  const id = window.crypto.getRandomValues(new Uint32Array(1))[0].toString(36)
  return `--${prefix}--${id}--`
}
