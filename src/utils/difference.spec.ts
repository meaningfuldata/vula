import { expect, it, describe } from 'vitest'
import { difference } from './'

describe('difference', () => {
  const a = new Set(['a', 'b', 'c'])

  describe('when both `Set` have the same members', () => {
    const b = new Set(['a', 'b', 'c'])

    it('returns an empty `Set`', () => {
      expect(difference(a, b)).to.deep.equal(new Set())
    })
  })

  describe('when first `Set` have completely different members than the second', () => {
    const b = new Set(['x', 'y', 'z'])

    it('returns a new `Set` with only the members of the first `Set`', () => {
      expect(difference(a, b)).to.deep.equal(new Set(['a', 'b', 'c']))
    })
  })

  describe('when first `Set` have some members of the second', () => {
    const b = new Set(['x', 'b', 'z'])

    it('returns a new `Set` with only the members of the first `Set` that are not in the second `Set`', () => {
      expect(difference(a, b)).to.deep.equal(new Set(['a', 'c']))
    })
  })
})
