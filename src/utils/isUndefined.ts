/**
 * Check if the value is a `undefined`.
 */
export default function isUndefined(value: any): value is undefined {
  return value === undefined
}
