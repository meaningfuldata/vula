import { isUndefined } from '.'

/**
 * Check if value is `null` or `undefined`.
 */
export default function isNil(value: any): value is null | undefined {
  return value === null || isUndefined(value)
}
