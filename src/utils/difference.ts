/**
 * Get the items in the first `Set` that are not in the second `Set`.
 */
export default function difference<T>(a: Set<T>, b: Set<T>): Set<T> {
  const result = new Set(a)
  for (let item of b) {
    result.delete(item)
  }
  return result
}
