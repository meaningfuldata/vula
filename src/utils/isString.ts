/**
 * Check if the value is a string.
 */
export default function isString(str: any): str is string {
  return typeof str === 'string'
}
