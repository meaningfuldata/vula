import type { Slot, VNode } from 'vue'

type Callback = (vNode: VNode) => void

const isFragment = (vNode: VNode): boolean =>
  typeof vNode.type === 'symbol' && vNode.type.toString() === 'Symbol(Fragment)'

// NOTE: For some reason, probably a bug, when using Vula with some apps it has the
// right `VNode` type when using the development version, but when the production
// build of those apps is used, the same `VNode` are not marked as fragments.
// TODO: investigate the problem further
const isFragmentLike = (vNode: VNode): boolean =>
  typeof vNode.type === 'symbol' &&
  vNode.type.toString() === 'Symbol()' &&
  !!vNode.children?.length

const traverseVNode = (vNode: VNode, callback: Callback) => {
  if (isFragment(vNode) || isFragmentLike(vNode)) {
    ;[vNode.children || []].flat().forEach(fragmentChild => {
      traverseVNode(fragmentChild as VNode, callback)
    })
  } else {
    callback(vNode)
  }
}

/**
 * Traverse the slot and apply a function to its children, even if they are inside a fragment.
 */
export default function traverseSlot(slot: Slot, callback: Callback) {
  const vNodes = slot()

  vNodes.forEach((child: VNode) => {
    traverseVNode(child, callback)
  })
}
