/**
 * Check if the value is a `Symbol`.
 */
export default function isSymbol(sym: any): sym is symbol {
  return typeof sym === 'symbol'
}
