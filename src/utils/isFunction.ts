import { isFunction } from 'lodash'

/**
 * Check if value is a function.
 */
export default isFunction
