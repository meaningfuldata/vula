import { findDuplicates } from './'
import { expect, it, describe } from 'vitest'

describe('findDuplicates', () => {
  it('returns the duplicated values', () => {
    const values = [2, 0, 1, 1, 3, 4, 0]
    expect(findDuplicates(values)).to.have.members([0, 1])
  })
})
