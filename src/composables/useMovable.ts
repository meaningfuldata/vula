import type { Ref } from 'vue'

/**
 * Horizontal or vertical
 */
type Axis = 'abcissa' | 'ordinate'

export interface Options {
  // TODO: currently is not used
  axes?: Array<Axis>
  // TODO: allow configuring throtle, debounce, etc.:
  // ```
  // onMouseMove: {
  //   throtle: 1000,
  //   handler: (event)
  // }
  // ```
  onMouseMove?: (event: MouseEvent) => void
}

const defaultOptions = {
  axes: ['abcissa', 'ordinate'] as Array<Axis>,
  onMouseMove: null
}

// TODO: rename
// TODO: refactor to increase reusability
// TODO: touch events
export default function useMovable(_elementRef: Ref, options: Options = {}) {
  const { onMouseMove } = { ...defaultOptions, ...options }

  const onMouseDown = (_event: MouseEvent) => {
    if (onMouseMove) {
      // TODO: check scroll with `passive: true|false`
      document.addEventListener('mousemove', onMouseMove)
    }
    document.addEventListener('mouseup', onMouseUp)
  }

  const onMouseEnter = (_event: MouseEvent) => {
    throw new Error('Not implemented yet')
  }

  const onMouseOver = (_event: MouseEvent) => {
    throw new Error('Not implemented yet')
  }

  const onMouseUp = (_event: MouseEvent) => {
    if (onMouseMove) {
      // TODO: check scroll with `passive: true|false`
      document.removeEventListener('mousemove', onMouseMove)
    }
    document.removeEventListener('mouseup', onMouseUp)
  }

  return {
    onMouseDown,
    onMouseEnter,
    onMouseOver,
    onMouseUp
  }
}
