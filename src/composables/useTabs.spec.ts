import {
  beforeAll,
  beforeEach,
  afterAll,
  expect,
  it,
  describe,
  test
} from 'vitest'
import { isReactive, isReadonly, isRef, reactive } from 'vue'
import { useTabs } from './'
import type { Tab } from '@/types/'
import sinon from 'sinon'

type Flush = 'pre' | 'sync'

const ID_REGEXP = /^--vula--.+--$/

describe('useTabs', () => {
  let exampleTabs: Array<Tab>

  beforeAll(() => {
    const random = () => [Math.round(Math.random() * 10000)]
    sinon.stub(crypto, 'getRandomValues').callsFake(random)
    sinon.stub(window, 'crypto').returns(crypto)
  })
  afterAll(() => sinon.resetBehavior())

  beforeEach(() => {
    exampleTabs = [
      { id: 'tab-0' },
      { id: 'tab-1' },
      { id: 'tab-2' },
      { id: 'tab-3' }
    ]
  })

  const testsWithFlush = (tests: Function) => {
    describe.skip('with `flush: "pre"`', () => {
      tests('pre')
    })

    describe('with `flush: "sync"`', () => {
      tests('sync')
    })
  }

  describe('options', () => {
    describe('when passing tabs without ID', () => {
      it('their IDs are auto-generated', () => {
        const noIdTabs = [{}, {}, {}]
        const { tabs } = useTabs({ tabs: noIdTabs })

        expect(tabs).to.have.lengthOf(3)
        tabs.forEach(tab => {
          expect(tab).to.have.property('id').that.match(ID_REGEXP)
        })
      })
    })

    describe('when passing a tab with an empty string as the ID', () => {
      it('is not changed', () => {
        const tab = { id: '' }
        const { tabs } = useTabs({ tabs: [tab, exampleTabs[0]] })

        expect(tabs[0].id).to.equal('')
      })
    })

    describe('when passing a tab with "0" as the ID', () => {
      it('is not changed', () => {
        const tab = { id: '0' }
        const { tabs } = useTabs({ tabs: [tab, exampleTabs[0]] })

        expect(tabs[0].id).to.equal('0')
      })
    })

    describe.skip('when passing tabs with duplicated IDs', () => {
      it('throws an `Error`', () => {
        const tab = { id: 'duplicated' }

        expect(() => {
          useTabs({ tabs: [tab, tab] })
        }).to.throw(/ID.*:.*duplicated/)
      })
    })

    describe('when passing non-reactive tabs', () => {
      describe('when no operation has been performed', () => {
        it('the returned `Ref` has the same value than the passed tabs', () => {
          const { tabs } = useTabs({ tabs: exampleTabs })
          expect(tabs).to.deep.equal(exampleTabs)
        })
      })

      it('changes to them do not affect the returned tabs', () => {
        const { tabs } = useTabs({ tabs: exampleTabs })

        // @ts-expect-error
        exampleTabs[0].id = 'NEW'
        expect(tabs[0].id).to.equal('tab-0')

        exampleTabs.pop()
        expect(exampleTabs).to.have.lengthOf(3)
        expect(tabs).to.have.lengthOf(4)
      })

      it('operations to the returned tabs do not affect them', () => {
        const { remove, tabs } = useTabs({ tabs: exampleTabs })

        // @ts-expect-error
        tabs[1] = { id: tabs[1].id, extraData: { some: 'value' } }
        // @ts-expect-error
        expect(tabs[1].extraData).to.deep.equal({ some: 'value' })
        // @ts-expect-error
        expect(exampleTabs[1].extraData).to.be.undefined

        remove(tabs[0].id)
        expect(tabs).to.have.lengthOf(3)
        expect(exampleTabs).to.have.lengthOf(4)
      })
    })

    describe('when passing reactive tabs', () => {
      it('they are modified in place', () => {
        const reactiveTabs = reactive(exampleTabs)
        const { tabs, remove } = useTabs({ tabs: reactiveTabs })

        remove(tabs[0].id)
        expect(tabs).to.have.lengthOf(3)
        expect(reactiveTabs).to.have.lengthOf(3)
      })
    })

    describe('when specifying the active tab ID', () => {
      it('that tab is active', () => {
        const { active } = useTabs({
          tabs: exampleTabs,
          activeId: exampleTabs[2].id
        })
        expect(active.value).to.deep.equal(exampleTabs[2])
      })
    })

    describe('when not specifying the active tab ID', () => {
      it('no tab is active', () => {
        const { active } = useTabs({ tabs: exampleTabs })
        expect(active.value).to.be.null
      })
    })

    describe('when no tab is passed', () => {
      it('no tab is active', () => {
        const { active } = useTabs()
        expect(active.value).to.be.null
      })
    })
  })

  describe('#activate', () => {
    it('activates the tab', () => {
      const { active, activate } = useTabs({ tabs: exampleTabs })
      expect(active.value).to.be.null

      activate(exampleTabs[2].id)
      expect(active.value).to.deep.equal(exampleTabs[2])
    })

    describe('when the tab ID is not found', () => {
      it('throws an `Error`', () => {
        const { activate } = useTabs({ tabs: exampleTabs })

        expect(() => {
          activate('activate-loca')
        }).to.throw('Tab with ID "activate-loca" not found')
      })
    })
  })

  describe('#active', () => {
    it('is a `Ref`', () => {
      const { active } = useTabs({ tabs: exampleTabs })
      expect(isRef(active)).to.be.true
    })

    it('is not read-only', () => {
      const { active } = useTabs({ tabs: exampleTabs })
      expect(isReadonly(active)).to.be.false
    })

    describe('setting a `Tab` as its value', () => {
      describe('when the tab exists', () => {
        it('activates that tab', () => {
          const { active } = useTabs({
            activeId: exampleTabs[2].id,
            tabs: exampleTabs
          })
          expect(active.value).to.deep.equal(exampleTabs[2])

          active.value = exampleTabs[1]
          expect(active.value).to.deep.equal(exampleTabs[1])
        })

        test.todo('updates that tab')
        test.todo('updates that tab in the `tabs` array')
      })

      describe('when the tab does not exist', () => {
        it('throws an `Error`', () => {
          const { active } = useTabs({ tabs: exampleTabs })

          expect(() => {
            active.value = { id: 'active-loca' }
          }).to.throw('Tab with ID "active-loca" not found')
        })
      })
    })

    // TODO: test setting other invalid values
    describe('when setting a `null` as its value', () => {
      it('throws an `Error`', () => {
        const { active } = useTabs({ tabs: exampleTabs })

        expect(() => {
          active.value = null
        }).to.throw('Cannot activate a tab without ID')
      })
    })
  })

  describe('#add', () => {
    const newTab = { id: 'new', title: 'New tab' }

    it('adds the tab as inactive by default', () => {
      const { add, active, tabs } = useTabs({ tabs: exampleTabs })

      add(newTab)

      expect(tabs).to.have.lengthOf(5)
      expect(tabs[4]).to.deep.equal(newTab)
      expect(active.value).to.be.null
    })

    describe('when passing the option `asActive` as `true`', () => {
      it('adds the tab as the active one', () => {
        const { add, active, tabs } = useTabs({ tabs: exampleTabs })

        add(newTab, { asActive: true })

        expect(tabs).to.have.lengthOf(5)
        expect(tabs[4]).to.deep.equal(newTab)
        expect(active.value).to.deep.equal(newTab)
      })
    })
  })

  describe.todo('#find', () => { })

  describe.todo('#groups', () => { })

  describe.todo('#isActive', () => { })

  describe('#isEmpty', () => {
    it('is a `Ref`', () => {
      const { isEmpty } = useTabs({ tabs: exampleTabs })
      expect(isRef(isEmpty)).to.be.true
    })

    it('is read-only', () => {
      const { isEmpty } = useTabs({ tabs: exampleTabs })
      expect(isReadonly(isEmpty)).to.be.true
    })

    it('returns `false` if there are tabs', () => {
      const { isEmpty } = useTabs({ tabs: exampleTabs })
      expect(isEmpty.value).to.be.false
    })

    it('returns `true` if there are not tabs', () => {
      const { isEmpty } = useTabs({ tabs: [] })
      expect(isEmpty.value).to.be.true
    })
  })

  describe.skip('#move', () => {
    describe('when the tab ID is not found', () => {
      it('throws an `Error`', () => {
        const { move } = useTabs({ tabs: exampleTabs })

        expect(() => {
          move('move-loca', { position: 1 })
        }).to.throw('Tab with ID "move-loca" not found')
      })
    })

    describe('when the new position is the same than the original one', () => {
      it('does not move the tab', () => {
        const { tabs, move } = useTabs({ tabs: exampleTabs })
        move(tabs[1].id, { position: 1 })

        expect(tabs[1]).to.deep.equal(exampleTabs[1])
      })
    })

    describe('when the new position is less than the original one', () => {
      it('moves the tab to the position', () => {
        const { tabs, move } = useTabs({ tabs: exampleTabs })
        expect(tabs[1]).to.deep.equal(exampleTabs[1])

        move(tabs[1].id, { position: 0 })
        expect(tabs[0]).to.deep.equal(exampleTabs[1])
      })

      it('adjust the position of the other tabs', () => {
        const { tabs, move } = useTabs({ tabs: exampleTabs })
        expect(tabs[0]).to.deep.equal(exampleTabs[0])

        move(tabs[1].id, { position: 0 })
        expect(tabs[1]).to.deep.equal(exampleTabs[0])
        expect(tabs[2]).to.deep.equal(exampleTabs[2])
        expect(tabs[3]).to.deep.equal(exampleTabs[3])
      })
    })

    describe('when the new position is greatear than the original one', () => {
      it('moves the tab to the position', () => {
        const { tabs, move } = useTabs({ tabs: exampleTabs })
        expect(tabs[1]).to.deep.equal(exampleTabs[1])

        move(tabs[1].id, { position: 2 })
        expect(tabs[2]).to.deep.equal(exampleTabs[1])
      })

      it('adjust the position of the other tabs', () => {
        const { tabs, move } = useTabs({ tabs: exampleTabs })
        expect(tabs[2]).to.deep.equal(exampleTabs[2])

        move(tabs[1].id, { position: 2 })
        expect(tabs[0]).to.deep.equal(exampleTabs[0])
        expect(tabs[1]).to.deep.equal(exampleTabs[2])
        expect(tabs[3]).to.deep.equal(exampleTabs[3])
      })
    })

    describe('when the position is negative', () => {
      it('throws an `Error`', () => {
        const { tabs, move } = useTabs({ tabs: exampleTabs })

        expect(() => {
          move(tabs[1].id, { position: -1 })
        }).to.throw('Cannot move the tab to a negative position (`-1`)')
      })
    })

    describe('when the position is greater than the number of tabs', () => {
      it('throws an `Error`', () => {
        const { tabs, move } = useTabs({ tabs: exampleTabs })

        expect(() => {
          move(tabs[1].id, { position: 100 })
        }).to.throw('Cannot move the tab to an invalid position (`100`)')
      })
    })

    test.todo('does not break the finding mechanism')
  })

  describe.todo('#query', () => { })

  describe('#remove', () => {
    describe('when tab is not closable', () => {
      test.todo('fails')
    })

    describe('when active tabs are being removed', () => {
      testsWithFlush((flush: Flush) => {
        it('the previous used tab is activated', () => {
          const { active, activate, remove } = useTabs({
            tabs: exampleTabs,
            flush
          })
          activate(exampleTabs[1].id)
          activate(exampleTabs[0].id)
          activate(exampleTabs[2].id)

          expect(active.value).to.deep.equal(exampleTabs[2])

          remove(exampleTabs[2].id)
          expect(active.value).to.deep.equal(exampleTabs[0])
          remove(exampleTabs[0].id)
          expect(active.value).to.deep.equal(exampleTabs[1])
          remove(exampleTabs[1].id)
          expect(active.value).to.deep.equal(exampleTabs[3])
          remove(exampleTabs[3].id)
          expect(active.value).to.be.null
        })

        describe('when no previous tabs have been used', () => {
          it('the tab positioned before the active one is activated', () => {
            const { active, remove } = useTabs({
              tabs: exampleTabs,
              activeId: exampleTabs[2].id,
              flush
            })
            expect(active.value).to.deep.equal(exampleTabs[2])

            remove(exampleTabs[2].id)
            expect(active.value).to.deep.equal(exampleTabs[1])
            remove(exampleTabs[1].id)
            expect(active.value).to.deep.equal(exampleTabs[0])
            remove(exampleTabs[0].id)
            expect(active.value).to.deep.equal(exampleTabs[3])
            remove(exampleTabs[3].id)
            expect(active.value).to.be.null
          })

          describe('when all tabs are positioned after the active one', () => {
            it('the next tab is activated', () => {
              const { active, remove } = useTabs({
                tabs: exampleTabs,
                activeId: exampleTabs[0].id,
                flush
              })
              expect(active.value).to.deep.equal(exampleTabs[0])

              remove(exampleTabs[0].id)
              expect(active.value).to.deep.equal(exampleTabs[1])
              remove(exampleTabs[1].id)
              expect(active.value).to.deep.equal(exampleTabs[2])
              remove(exampleTabs[2].id)
              expect(active.value).to.deep.equal(exampleTabs[3])
              remove(exampleTabs[3].id)
              expect(active.value).to.be.null
            })
          })
        })
      })
    })

    describe('when the tab has been activated several times', () => {
      test.todo('does not try to activate it after removal')
    })
  })

  describe('#tabs', () => {
    it('are reactive', () => {
      const { tabs } = useTabs({ tabs: exampleTabs })
      expect(isReactive(tabs)).to.be.true
    })

    it('are not a `Ref`', () => {
      const { tabs } = useTabs({ tabs: exampleTabs })
      expect(isRef(tabs)).to.be.false
    })

    it('are not read-only', () => {
      const { tabs } = useTabs({ tabs: exampleTabs })
      expect(isReadonly(tabs)).to.be.false
    })

    describe('when there are no tabs', () => {
      testsWithFlush((flush: Flush) => {
        describe('when a tab is pushed', () => {
          it('that tab is not activated', () => {
            const { tabs, active } = useTabs({ flush })
            tabs.push(exampleTabs[1])
            expect(active.value).to.be.null
          })
        })

        describe('when several tabs are pushed', () => {
          it('none of them is activated', () => {
            const { tabs, active } = useTabs({ flush })
            tabs.push(...exampleTabs)
            expect(active.value).to.be.null
          })
        })
      })
    })

    describe('when there are other tabs', () => {
      describe('when a tab is pushed', () => {
        it('the active tab does not change', () => {
          const { tabs, active } = useTabs({
            activeId: exampleTabs[3].id,
            tabs: exampleTabs
          })
          expect(() => tabs.push(exampleTabs[1])).to.not.change(() => active)
        })
      })

      describe('when several tabs are pushed', () => {
        it('the active tab does not change', () => {
          const { tabs, active } = useTabs({
            activeId: exampleTabs[3].id,
            tabs: exampleTabs
          })
          expect(() => tabs.push(...exampleTabs)).to.not.change(() => active)
        })
      })
    })

    describe.todo('when a tab is modified', () => { })

    describe('when a tab is removed', () => {
      describe('when it is the active tab', () => {
        testsWithFlush((flush: Flush) => {
          it('activates other tab', () => {
            const { tabs, active } = useTabs({
              activeId: exampleTabs[3].id,
              tabs: exampleTabs,
              flush
            })
            expect(active.value).to.deep.equal(exampleTabs[3])

            tabs.pop()
            expect(active.value).to.deep.equal(exampleTabs[2])
          })
        })
      })

      describe('when it is not the active tab', () => {
        it('does not change the active tab', () => {
          const { tabs, active } = useTabs({
            activeId: exampleTabs[3].id,
            tabs: exampleTabs
          })
          expect(() => tabs.splice(1, 1)).to.not.change(() => active)
        })
      })
    })

    describe('when several tabs are removed', () => {
      describe('when it includes the active tab', () => {
        testsWithFlush((flush: Flush) => {
          it('activates other', () => {
            const { tabs, active } = useTabs({
              activeId: exampleTabs[3].id,
              tabs: exampleTabs,
              flush
            })
            expect(active.value).to.deep.equal(exampleTabs[3])

            tabs.splice(2, 2)
            expect(active.value).to.deep.equal(exampleTabs[1])
          })
        })
      })

      describe('when it does not include the active tab', () => {
        it('does not change the active one', () => {
          const { tabs, active } = useTabs({
            activeId: exampleTabs[3].id,
            tabs: exampleTabs
          })
          expect(() => tabs.splice(1, 3)).to.not.change(() => active)
        })
      })
    })

    describe('when all tabs are removed', () => {
      it('sets the active tab as `null`', () => {
        const { tabs, active } = useTabs({
          activeId: exampleTabs[3].id,
          tabs: exampleTabs
        })
        expect(active.value).to.deep.equal(exampleTabs[3])

        tabs.splice(0, 4)
        expect(active.value).to.be.null
      })
    })

    // TODO: when there are tabs, and more are added
  })

  describe('#wasActive', () => {
    testsWithFlush((flush: Flush) => {
      it('returns `true` if the tab has been active previously', () => {
        const { activate, wasActive } = useTabs({
          activeId: exampleTabs[3].id,
          tabs: exampleTabs,
          flush
        })

        activate(exampleTabs[0].id)
        expect(wasActive(exampleTabs[0].id)).to.be.true
        expect(wasActive(exampleTabs[3].id)).to.be.true
      })

      it('returns `true` if the tab was active but removed', () => {
        const { activate, remove, wasActive } = useTabs({
          tabs: exampleTabs,
          flush
        })

        activate(exampleTabs[0].id)
        remove(exampleTabs[0].id)
        expect(wasActive(exampleTabs[0].id)).to.be.true
      })
    })

    it('returns `false` if the tab has not been active previously', () => {
      const { activate, wasActive } = useTabs({ tabs: exampleTabs })

      activate(exampleTabs[0].id)
      expect(wasActive(exampleTabs[1].id)).to.be.false
      expect(wasActive(exampleTabs[2].id)).to.be.false
    })
  })

  describe('#wasRemoved', () => {
    testsWithFlush((flush: Flush) => {
      it('returns `true` if the tab has been removed', () => {
        const { remove, wasRemoved } = useTabs({
          tabs: exampleTabs,
          flush
        })

        remove(exampleTabs[0].id)
        expect(wasRemoved(exampleTabs[0].id)).to.be.true
      })

      describe('when the tab has been removed and added again', () => {
        it('returns `false`', () => {
          const { remove, tabs, wasRemoved } = useTabs({
            tabs: exampleTabs,
            flush
          })

          remove(exampleTabs[0].id)
          expect(wasRemoved(exampleTabs[0].id)).to.be.true
          tabs.push(exampleTabs[0])

          expect(wasRemoved(exampleTabs[0].id)).to.be.false
        })
      })
    })

    it('returns `false` if the tab has not been removed', () => {
      const { wasRemoved } = useTabs({ tabs: exampleTabs })

      expect(wasRemoved(exampleTabs[0].id)).to.be.false
    })
  })
})
