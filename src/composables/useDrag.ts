import { ref, Ref } from 'vue'

export interface Options {
  target: Ref<any>
  dataType?: string
}

const defaultOptions = {
  dataType: 'vula'
}

/*
 * TODO:
 *  - optional target: use event?
 *  - if target, add `draggable` attribute
 *  - if target, add listeners automatically
 *  - do not depend on `id`
 *  - allow setting effects
 */
export default function useDrag(options: Options) {
  const { target, dataType } = { ...defaultOptions, ...options }

  const isDragging = ref(false)

  const onDragEnd = (event: DragEvent) => {
    isDragging.value = false

    // FIXME
    // target.value.classList.remove('vula--dragging')
  }

  const onDragStart = (event: DragEvent) => {
    isDragging.value = true

    if (event.dataTransfer) {
      event.dataTransfer.setData(dataType, target.value.id)
      // TODO
      // event.dataTransfer.setData('text/plain', target.value.id)
      // event.dataTransfer.setData('text/html', target.value.id)
      // event.dataTransfer.setData('text/uri-list', target.value.id)
      event.dataTransfer.effectAllowed = 'move'
    }

    // Hide the original element to reflect that it is being dragged
    // setTimeout(() => target.value.classList.add('vula--dragging'), 0)
  }

  return {
    isDragging,
    onDragEnd,
    onDragStart
  }
}
