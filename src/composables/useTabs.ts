import { computed, isReactive, isRef, reactive, ref, watch } from 'vue'
import type {
  ComputedRef,
  UnwrapNestedRefs as Reactive,
  WritableComputedRef
} from 'vue'
import { clone, deepIdentify, difference, isNil, isString } from '@/utils/'
import type {
  Anonymous,
  ID,
  MaybeReactive,
  MaybeRef,
  Tab,
  TabQuery
} from '@/types/'

// TODO: use a Set, since tabs cannot be duplicated?
type AnonymousTabs = Anonymous<Tab>[]
type Tabs = Tab[]

// TODO: use `current` or similar to be less ambiguous instead of `active`,
// since tabs could be doing something on the background, so, they could be called active

// TODO: option to use first or last tab as default active tab

export interface Options {
  /**
   * The ID of the initial active tab.
   * When the ID is not reactive, it is used to initialise the active tab.
   * When the ID is a `Ref`, its value is bound to the active tab ID.
   */
  activeId?: MaybeRef<ID | null>
  /**
   * The initial tabs.
   * When the object is not reactive, a copy of these tabs is stored internally,
   * and it is that copy the only one which receives the operations.
   * When the object is reactive, operations are performed in place.
   */
  tabs?: MaybeReactive<AnonymousTabs>
  /**
   * How Vue reactivity system should flushes effects.
   * @see https://v3.vuejs.org/guide/reactivity-computed-watchers.html#effect-flush-timing
   */
  flush?: 'pre' | 'post' | 'sync'
}

export interface AddOptions {
  /**
   * Option to add the tab as the new active one.
   */
  asActive?: boolean
}
export interface MoveOptions { }
export interface RemoveOptions { }

export interface UseTabs {
  activate: (tab: Tab | ID) => void
  active: WritableComputedRef<Tab | null>
  add: (tab: Tab, options?: AddOptions) => void
  find: (tab: Tab | ID) => Tab | null
  findPosition: (tab: Tab | ID) => number | null
  isActive: (tab: Tab | ID) => boolean
  isEmpty: ComputedRef<boolean>
  move: (tab: Tab | ID, options?: MoveOptions) => void
  query: (query: TabQuery) => Tab[]
  remove: (tab: Tab | ID, options?: RemoveOptions) => void
  tabs: Reactive<Tab[]>
  wasActive: (tab: Tab | ID) => boolean
  wasRemoved: (tab: Tab | ID) => boolean
}

/**
 * Functions to manage tabs
 * @param tabGroup Tab group, with options, or its ID TODO
 */
export default function useTabs(options: Options = {}): UseTabs {
  const {
    activeId: seedActiveId,
    tabs: seedTabs,
    flush
  } = {
    activeId: null,
    tabs: [],
    ...options
  }

  /**
   * The ID of the active tab.
   */
  const activeId = isRef(seedActiveId) ? seedActiveId : ref(seedActiveId)

  /**
   * All the tabs.
   * When passing a non-reactive array, it is cloned, to avoid modifying
   * the original tabs, and converted to a reactive object.
   */
  // TODO: do not clone the tabs (to not include a big dependency on the project),
  // since users can clone the tabs themselves if they do not want side-effects
  // TODO: shallowReactive?
  const tabs: Reactive<Tabs> = isReactive(seedTabs)
    ? deepIdentify(seedTabs)
    : reactive(deepIdentify(clone(seedTabs)))

  /**
   * The tabs that have been activated.
   *
   * It is reactive to trigger effects when using `wasActive`.
   */
  const activatedIds: Reactive<Set<ID>> = reactive(new Set())

  /**
   * The tabs that have been removed.
   *
   * It is reactive to trigger effects when using `wasRemoved`.
   */
  const removedIds: Reactive<Set<ID>> = reactive(new Set())

  /**
   * Stack to determine the next active tab when the current one is removed.
   * The last one is the active tab.
   */
  const activeStack: Set<ID> = new Set([])

  /**
   * Return the ID of a tab or tab ID
   */
  const getId = (tab: Tab | ID): ID | null =>
    tab ? (isString(tab) ? tab : tab.id) : null

  /**
   * Find a tab in the `tabs` array.
   *
   * Ignores empty items that could exist when manipulating the `tabs` array and using `flush: 'sync'`.
   */
  const find = (tab: Tab | ID): Tab | null => {
    return tabs.find(t => t?.id === getId(tab)) || null
  }

  /**
   * Find the position of a tab in the `tabs` array.
   *
   * Ignores empty items that could exist when manipulating the `tabs` array and using `flush: 'sync'`.
   */
  const findPosition = (tab: Tab | ID): number | null => {
    const position = tabs.findIndex(t => t?.id === getId(tab))
    return position >= 0 ? position : null
  }

  /**
   * Activate the tab
   */
  const activate = (tab: Tab | ID): void => {
    // TODO: stricter check
    if (!tab) {
      throw new Error(`Cannot activate a tab without ID`)
    }
    const found = find(tab)
    if (!found) {
      throw new Error(`Tab with ID "${getId(tab)}" not found`)
    }

    activeId.value = getId(found)
  }

  const activateLast = () => activate([...activeStack][activeStack.size - 1])

  /**
   * Active tab, or `null` if there are no tabs.
   */
  const active: WritableComputedRef<Tab> = computed({
    // TODO: type: Tab | null
    get: (): Tab => find(activeId.value as ID) as Tab,
    // TODO: update the tab attributes too?
    set: (tab: Tab | ID) => activate(tab)
  })

  /**
   * Get the ID of the tabs.
   *
   * To avoid triggering unnecessary effects:
   *  - Ignores temporary placeholders (`undefined`).
   *  - Ignores duplicates.
   */
  const getIds = (tabs: Tab[]): Set<ID> => {
    return tabs.reduce(
      (ids: Set<ID>, tab: Tab) => (tab && tab.id ? ids.add(tab.id) : ids),
      new Set()
    )
  }

  watch(
    activeId,
    (id: unknown) => {
      // TODO: fail if null but there are tabs
      if (id) {
        // Add the tab to the stack, but ensure that only one exists
        activeStack.delete(id as ID)
        activeStack.add(id as ID)

        // Add the tab to the list of tabs that have been active
        activatedIds.add(id as ID)
      }
    },
    { immediate: true, flush }
  )

  // TODO: explain rationale of removal
  watch(
    () => getIds(tabs),
    (ids: Set<ID>, previousIds: Set<ID> | undefined) => {
      const size = ids.size

      // When no tab is available, none can be active
      if (!size) {
        activeId.value = null

        // When there are tabs, but nothing has been initialized, decide the
        // activation order after removal (`activeStack`)
      } else if (!previousIds) {
        let stack: Set<ID> = ids

        if (activeId.value) {
          const position = findPosition(activeId.value)!
          const before = [...ids].slice(0, position + 1),
            after = [...ids].slice(position + 1)

          stack = new Set(
            position === 0
              ? // Activate the tab after the active tab
              [...after.reverse(), ...before]
              : // Activate the tab before the active tab
              [...after, ...before]
          )
        }

        stack.forEach((id: ID) => activeStack.add(id))

        // When tabs have been already initialised
      } else if (previousIds) {
        const removedPreviousIds: Set<ID> = difference(previousIds, ids)

        // TODO: added and removed at the same time

        // When tabs have been removed
        if (removedPreviousIds.size) {
          removedPreviousIds.forEach((id: ID) => {
            // Delete the removed tabs from the stack to determine the next active tab
            activeStack.delete(id)
            // Store the tabs that were removed
            removedIds.add(id)
          })

          // Activate the last tab in the stack
          activateLast()
        } else {
          // If tabs have been added
          const addedIds: Set<ID> = difference(ids, previousIds)
            ;[...addedIds].reverse().forEach((id: ID) => {
              // Include them, in reverse order, in `activeStack` to keep the order TODO: test
              activeStack.add(id)
              // Drop them from the list of removed tabs
              removedIds.delete(id)
            })
        }
      }
    },
    { immediate: true, flush }
  )

  /**
   * Add a new tab.
   *
   * By default, tabs are added at the end.
   *
   * TODO: allow choosing the position
   */
  const add = (tab: Tab, options: AddOptions = {}): void => {
    const { asActive } = {
      asActive: false,
      ...options
    }

    tabs.push(tab)

    if (asActive) {
      activate(tab)
    }
  }

  const move = (_tab: Tab | ID, _options?: MoveOptions): void => {
    throw new Error('Not implemented yet')
  }

  /**
   * Close the tab.
   *
   * It activates a previously used tab in case that the tab to close is active.
   *
   * TODO: allow removing more than 1 tab at once
   */
  const remove = (tab: Tab | ID, options?: RemoveOptions): void => {
    if (options) {
      throw new Error('Options not implemented yet')
    }

    const position = findPosition(tab)
    if (isNil(position)) {
      throw new Error(
        `Cannot remove tab with ID "${getId(tab)}" since it is not found`
      )
    }
    tabs.splice(position, 1)
  }

  const query = (_query: TabQuery): Tab[] => {
    throw new Error('Not implemented yet')
  }

  /**
   * Is the tab array empty?
   */
  const isEmpty = computed((): boolean => !tabs.length)

  /**
   * Is this tab the active one?
   */
  const isActive = (tab: Tab | ID): boolean => getId(tab) === activeId.value

  /**
   * Was this tab active previously?
   *
   * It considers current tabs and those which has been removed.
   * It is reactive: it will trigger an effect if a tab is activated.
   */
  const wasActive = (tab: Tab | ID): boolean => {
    const tabId = getId(tab)
    return tabId ? activatedIds.has(tabId) : false
  }

  /**
   * Was this tab removed?
   *
   * If the tab has been removed, but it is added again, it is considered as not removed.
   * It is reactive: it will trigger an effect if a tab is removed.
   */
  const wasRemoved = (tab: Tab | ID): boolean => {
    const tabId = getId(tab)
    return tabId ? removedIds.has(tabId) : false
  }

  return {
    activate,
    active,
    add,
    find,
    findPosition,
    isActive,
    isEmpty,
    move,
    query,
    remove,
    tabs,
    wasActive,
    wasRemoved
  }
}
