import { watch, Ref } from 'vue'

export interface Options {
  target: Ref<any>
  dataType?: string
  onDrop?: (event: DragEvent, data: any, target: HTMLElement) => void
}

const defaultOptions = {
  dataType: 'vula'
}

/*
 * TODO:
 *  - optional target: use event.target for `dragenter` and `dragleave`
 *  - if target, add listeners automatically
 *  - allow setting effects
 */
export default function useDrop(options: Options) {
  const { target, dataType } = { ...defaultOptions, ...options }

  // Has entered into a droppable area?
  let hasEntered = false
  // Has leaved a droppable area?
  let hasLeft = true

  /**
   * Create a custom event that inherits the properties from a native event.
   * These custom events work as expected regarding their propagation.
   *
   * The solution has been inspired by [Dragster](https://bensmithett.github.io/dragster/)
   * TODO: test edge cases to ensure
   */
  const createEvent = (name: string, nativeEvent: DragEvent) => {
    const event = new CustomEvent(name, {
      bubbles: true,
      cancelable: true
    })

    const properties = [
      ...Object.keys(MouseEvent.prototype),
      ...Object.keys(DragEvent.prototype)
    ]

    const data = properties.reduce((acc: any, property) => {
      acc[property] = (nativeEvent as any)[property]
      return acc
    }, {})

    return Object.assign(event, data)
  }

  const vulaDragEnter = (nativeEvent: DragEvent) => {
    if (hasEntered) {
      hasLeft = false
    } else {
      hasEntered = true

      const event = createEvent('vula:drag-enter', nativeEvent)
      target.value.dispatchEvent(event)
    }
  }

  const vulaDragLeave = (nativeEvent: DragEvent) => {
    if (!hasLeft) {
      hasLeft = true
    } else if (hasEntered) {
      hasEntered = false
    }

    if (!hasEntered && hasLeft) {
      const event = createEvent('vula:drag-leave', nativeEvent)
      target.value.dispatchEvent(event)
    }
  }

  const vulaReset = (nativeEvent: DragEvent) => {
    hasEntered = false
    hasLeft = true
  }

  // TODO: better way
  watch(
    target,
    (element: any) => {
      if (element) {
        element.addEventListener('vula:drag-enter', onDragEnter, false)
        element.addEventListener('vula:drag-leave', onDragLeave, false)
        element.addEventListener('dragenter', vulaDragEnter, false)
        element.addEventListener('dragleave', vulaDragLeave, false)
        element.addEventListener('drop', vulaReset, false)
      }
    },
    { immediate: true }
  )

  const onEvent = (callback: (event: DragEvent, data: any) => void) => {
    return (event: DragEvent): void => {
      const data = event.dataTransfer!.getData(dataType)
      if (data) {
        event.preventDefault()
        event.stopPropagation()
        callback(event, data)
      }
    }
  }

  const enter = () => target.value.classList.add('vula--drag-entered')
  const leave = () => target.value.classList.remove('vula--drag-entered')

  /**
   * When a dragged object enters an area that supports droping objects.
   */
  const onDragEnter = onEvent((event: DragEvent, data: any) => {
    enter()
  })

  /**
   * When a dragged object leaves an area that supports droping objects.
   */
  const onDragLeave = onEvent((event: DragEvent, data: any) => {
    leave()
  })

  /**
   * When a dragged object moves inside an area that supports droping objects.
   */
  const onDragOver = onEvent((event: DragEvent, data: any) => {
    event.dataTransfer!.dropEffect = 'move'
  })

  // TODO: remove
  const calculatePointer = (event: DragEvent | MouseEvent) => {
    const { x, y } = target.value.getBoundingClientRect(),
      { clientX, clientY } = event,
      top = clientY - y,
      left = clientX - x

    return {
      top,
      left
    }
  }

  const traverseUpUntil = (
    element: HTMLElement,
    check: (element: HTMLElement) => boolean
  ): null | HTMLElement => {
    return check(element)
      ? element
      : element.parentElement
      ? traverseUpUntil(element.parentElement, check)
      : null
  }

  const findDroppable = (element: HTMLElement): null | HTMLElement => {
    return traverseUpUntil(element, ({ classList }: HTMLElement) => {
      return classList.contains('vula--droppable')
    })
  }

  /**
   * Infer the real target of a drag & drop event.
   * TODO: calculate best target?
   */
  const inferTarget = (event: DragEvent): null | HTMLElement => {
    const droppable = findDroppable(event.target as HTMLElement)

    return droppable
  }

  /**
   * When a dragged object is dropped inside an area that support it.
   */
  const onDrop = onEvent((event: DragEvent, data: any) => {
    leave()

    if (options.onDrop) {
      const target = inferTarget(event)
      if (target) {
        options.onDrop(event, data, target)
      } else {
        throw new Error('Target not found')
      }
    }
  })

  return {
    onDragEnter,
    onDragOver,
    onDragLeave,
    onDrop
  }
}
