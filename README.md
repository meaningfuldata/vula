# Vula

**Vula is still experimental**: some breaking changes may be released as minor versions.

Vula is a library for creating dynamic complex **Vu**e **la**youts.

## Features

- Lightweight (~12 KiB. gzipped)

### Panes

- [x] Resizable.
- [ ] Draggables and droppables.
- [ ] Maximizables.
- [ ] Can have tabs or be empty.
- [ ] Pin to the edges.
- [ ] Customizables.

### Tabs

- [ ] Draggables and droppables.
- [ ] Maximizables.
- [x] Closables.
- [ ] Sortables.
- [ ] Can be extracted to a new window.
- [x] Customizables.

## Installation

## Usage

## Examples

The `examples` folder includes an application that shows several features.

- NOTE: this examples are not finished yet.

## Alternatives

On Vue:

- [splitpanes](https://www.npmjs.com/package/splitpanes): an excellent project, but focused on panes only
  On React.
- [rc-dock](https://www.npmjs.com/package/rc-dock)

## Collaboration

## License

Apache 2.0

## Authors
